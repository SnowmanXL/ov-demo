package nl.snowman.race.messages;

import nl.snowman.race.model.Team;

public class ScoreListing {
    private static final String type = "Game_Score";
    private static final int MAX_GAME_SCORE = 1000;
    private int teamA = 0;
    private int teamB = 0;

    public void scoreFor(Team team, int score) {
        switch (team) {
            case A:
                teamA += score;
                break;
            case B:
                teamB += score;
                break;
        }
    }

    public Team getWinner() {
        Team winner = null;
        if (teamA >= MAX_GAME_SCORE) {
            winner = Team.A;
        } else if (teamB >= MAX_GAME_SCORE) {
            winner = Team.B;
        }
        return winner;
    }

    public boolean anyoneWon() {
        return getWinner() != null;
    }

    public int getTeamA() {
        return teamA;
    }

    public int getTeamB() {
        return teamB;
    }

    public String getType() {
        return type;
    }

    @Override
    public String toString() {
        return "ScoreListing{" +
                "type='" + type + '\'' +
                ", teamA=" + teamA +
                ", teamB=" + teamB +
                '}';
    }
}
