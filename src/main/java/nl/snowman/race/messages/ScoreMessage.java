package nl.snowman.race.messages;

public class ScoreMessage {
    String team;

    public ScoreMessage(String team) {
        this.team = team;
    }

    public ScoreMessage() {
    }

    public String getTeam() {
        return team;
    }

    void setTeam(String team) {
        this.team = team;
    }

    @Override
    public String toString() {
        return "ScoreMessage{" +
                "team='" + team + '\'' +
                '}';
    }
}