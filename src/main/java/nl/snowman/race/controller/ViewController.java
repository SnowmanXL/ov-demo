package nl.snowman.race.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ViewController {

    @RequestMapping({ "/console"  })
    public String index() {
        return "forward:/index.html";
    }
}
