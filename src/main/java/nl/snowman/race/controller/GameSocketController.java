package nl.snowman.race.controller;


import nl.snowman.race.messages.ScoreListing;
import nl.snowman.race.messages.ScoreMessage;
import nl.snowman.race.model.Player;
import nl.snowman.race.model.Team;
import nl.snowman.race.service.SocketEventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

@Controller
@RestController
public class GameSocketController {

    private static final Logger LOGGER = LoggerFactory.getLogger(GameSocketController.class);
    private static ScoreListing scores = new ScoreListing();
    private static AtomicBoolean gameIsStarted = new AtomicBoolean(false);

    @Autowired
    private SimpMessagingTemplate webSocket;

    @Autowired
    SocketEventListener listener;

    @RequestMapping(path = "/reset")
    public void resetGame() {
        scores = new ScoreListing();
    }

    @RequestMapping(path = "/startgame")
    public void startGame() {
        gameIsStarted.compareAndSet(false, true);
    }

    @RequestMapping(path = "/getTeam")
    public Team getTeam() {
        return getNewTeam();
    }

    @MessageMapping("/click")
    public void clickCount(@Payload Message<ScoreMessage> rawMessage) {
        Team team = Team.valueOf(rawMessage.getPayload().getTeam());
        LOGGER.info("Clicked {}", rawMessage);
        if (gameIsStarted.get()) {
            scores.scoreFor(team, scoreAdd(getTeamCount(team)));

            if (scores.anyoneWon()) {
                gameIsStarted.compareAndSet(true, false);
            }
        }
        LOGGER.info("Score {}", scores);
        webSocket.convertAndSend("/topic/game", scores);
    }

    private int scoreAdd(int playerCount) {
        int score = 10 / ++playerCount;
        return score == 0 ? 1 : score;
    }

    private Team getNewTeam() {
        return getTeamCount(Team.A) <= getTeamCount(Team.B) ? Team.A : Team.B;
    }

    private int getTeamCount(Team team) {
        return Math.toIntExact(Optional.ofNullable(getPlayerCountPerTeam().get(team))
                .orElse(0L));
    }

    private Map<Team, Long> getPlayerCountPerTeam() {
        return listener.getPlayers()
                .stream()
                .collect(Collectors.groupingBy(Player::getTeamName, Collectors.counting()));
    }

}
