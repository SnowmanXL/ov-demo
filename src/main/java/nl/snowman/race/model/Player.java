package nl.snowman.race.model;

public class Player {
    private String id;
    private Team team;

    public Player(String id, Team team) {
        this.id = id;
        this.team = team;
    }

    public boolean isEqualTo(String id) {
        return this.id.equals(id);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Team getTeamName() {
        return team;
    }

    public void setTeamName(Team teamName) {
        this.team = teamName;
    }
}
