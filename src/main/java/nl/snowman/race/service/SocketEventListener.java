package nl.snowman.race.service;

import nl.snowman.race.model.Player;
import nl.snowman.race.model.Team;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;
import org.springframework.web.socket.messaging.SessionSubscribeEvent;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

@Service
public class SocketEventListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(SocketEventListener.class);

    private List<Player> players = new CopyOnWriteArrayList<>();
    public List<Player> getPlayers() {
        return players;
    }

    @EventListener
    public void onSessionSubscribedEvent(SessionSubscribeEvent event) {
        StompHeaderAccessor sha = StompHeaderAccessor.wrap(event.getMessage());
        List<String> team = sha.getNativeHeader("team");
        if (team != null && team.size() > 0) {
            players.add(new Player(sha.getSessionId(), Team.valueOf(team.get(0))));
        }
        LOGGER.info("Session connect: number of active connections: {}", players);
    }

    @EventListener
    public void onSessionDisconnectEvent(SessionDisconnectEvent event) {
        StompHeaderAccessor sha = StompHeaderAccessor.wrap(event.getMessage());
        players.removeIf(p -> p.isEqualTo(sha.getSessionId()));
        LOGGER.info("Session disconnect: number of active connections: {}", players);
    }

}
