webpackJsonp(["main"],{

/***/ "../../../../../src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../../../../../src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>\r\n"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = (function () {
    function AppComponent() {
        this.title = 'app';
    }
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-root',
            template: __webpack_require__("../../../../../src/app/app.component.html")
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export socketProvider */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__game_client_game_client_component__ = __webpack_require__("../../../../../src/app/game-client/game-client.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__game_config_client_game_config_client_component__ = __webpack_require__("../../../../../src/app/game-config-client/game-config-client.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__stomp_ng2_stompjs__ = __webpack_require__("../../../../@stomp/ng2-stompjs/@stomp/ng2-stompjs.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_sockjs_client__ = __webpack_require__("../../../../sockjs-client/lib/entry.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_sockjs_client___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_sockjs_client__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__services_game_api_service__ = __webpack_require__("../../../../../src/app/services/game-api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__angular_common_http__ = __webpack_require__("../../../common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__progress_bar_animated_progress_bar_animated_component__ = __webpack_require__("../../../../../src/app/progress-bar-animated/progress-bar-animated.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ng_bootstrap_ng_bootstrap__ = __webpack_require__("../../../../@ng-bootstrap/ng-bootstrap/index.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};












var appRoutes = [
    { path: '', component: __WEBPACK_IMPORTED_MODULE_3__game_client_game_client_component__["a" /* GameClientComponent */] },
    { path: 'console', component: __WEBPACK_IMPORTED_MODULE_4__game_config_client_game_config_client_component__["a" /* GameConfigClientComponent */] }
];
function socketProvider() {
    return new __WEBPACK_IMPORTED_MODULE_7_sockjs_client___default.a('/pulltherope');
}
var stompConfig = {
    url: socketProvider,
    headers: {
        login: '',
        passcode: ''
    },
    heartbeat_in: 0,
    heartbeat_out: 20000,
    reconnect_delay: 5000,
    // Will log diagnostics on console
    debug: true
};
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_3__game_client_game_client_component__["a" /* GameClientComponent */],
                __WEBPACK_IMPORTED_MODULE_4__game_config_client_game_config_client_component__["a" /* GameConfigClientComponent */],
                __WEBPACK_IMPORTED_MODULE_10__progress_bar_animated_progress_bar_animated_component__["a" /* ProgressBarAnimatedComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_router__["a" /* RouterModule */].forRoot(appRoutes),
                __WEBPACK_IMPORTED_MODULE_9__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_11__ng_bootstrap_ng_bootstrap__["a" /* NgbModule */].forRoot()
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_6__stomp_ng2_stompjs__["b" /* StompService */],
                {
                    provide: __WEBPACK_IMPORTED_MODULE_6__stomp_ng2_stompjs__["a" /* StompConfig */],
                    useValue: stompConfig
                },
                __WEBPACK_IMPORTED_MODULE_8__services_game_api_service__["a" /* GameService */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "../../../../../src/app/game-client/game-client.component.html":
/***/ (function(module, exports) {

module.exports = "\r\n<div class=\"container\">\r\n  <div class=\"row\">\r\n    <div class=\"container\">\r\n      <table style=\"height: 100px;\">\r\n        <tbody>\r\n        <tr>\r\n          <th class=\"align-middle\">Team</th>\r\n          <td class=\"align-middle\"><h1>{{ teamName }}</h1></td>\r\n        </tr>\r\n        </tbody>\r\n      </table>\r\n      <button type=\"button\" class=\"btn btn-primary btn-xlarge btn-block\" (click)=\"score()\">Score!</button>\r\n    </div>\r\n    <div *ngIf=\"showWinImage\" class=\"container\">\r\n      <hr>\r\n      <h3>You won! Time to party</h3>\r\n      <img  src=\"https://media1.tenor.com/images/165d65dcedc3845a647836ee21b15854/tenor.gif\" class=\"img-fluid\" alt=\"Responsive image\">\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n"

/***/ }),

/***/ "../../../../../src/app/game-client/game-client.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GameClientComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__stomp_ng2_stompjs__ = __webpack_require__("../../../../@stomp/ng2-stompjs/@stomp/ng2-stompjs.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_game_api_service__ = __webpack_require__("../../../../../src/app/services/game-api.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var GameClientComponent = (function () {
    function GameClientComponent(_stompService, stageServices) {
        var _this = this;
        this._stompService = _stompService;
        this.stageServices = stageServices;
        this.showWinImage = false;
        this.on_next = function (message) {
            // console.log(message);
            var msg = JSON.parse(message.body);
            console.log('json', msg);
            switch (msg.type) {
                case 'Game_Score':
                    _this.showWinImage = (_this.teamName === msg.winner);
                    break;
                case 'Reset':
                    if (_this.showWinImage === true) {
                        _this.showWinImage = !('reset' === msg.resp);
                    }
                    break;
            }
        };
    }
    GameClientComponent.prototype.ngOnInit = function () {
        this.subscribed = false;
        this.getTeamName();
    };
    GameClientComponent.prototype.ngOnDestroy = function () {
        this.unsubscribe();
    };
    GameClientComponent.prototype.subscribe = function () {
        if (this.subscribed) {
            return;
        }
        this.messages = this._stompService.subscribe('/topic/game', { team: this.teamName });
        this.subscription = this.messages.subscribe(this.on_next);
        this.subscribed = true;
    };
    GameClientComponent.prototype.unsubscribe = function () {
        if (!this.subscribed) {
            return;
        }
        // This will internally unsubscribe from Stomp Broker
        // There are two subscriptions - one created explicitly, the other created in the template by use of 'async'
        this.subscription.unsubscribe();
        this.subscription = null;
        this.messages = null;
        this.subscribed = false;
    };
    GameClientComponent.prototype.score = function () {
        this._stompService.publish('/app/click', "{\"team\": \"" + this.teamName + "\"}");
    };
    GameClientComponent.prototype.getTeamName = function () {
        var _this = this;
        this.stageServices.getTeam().subscribe(function (data) {
            console.log('Data: ', data);
            _this.teamName = data;
            _this.subscribe();
        });
    };
    GameClientComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-game-client',
            template: __webpack_require__("../../../../../src/app/game-client/game-client.component.html")
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__stomp_ng2_stompjs__["b" /* StompService */], __WEBPACK_IMPORTED_MODULE_2__services_game_api_service__["a" /* GameService */]])
    ], GameClientComponent);
    return GameClientComponent;
}());



/***/ }),

/***/ "../../../../../src/app/game-config-client/game-config-client.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\r\n  <div class=\"row\">\r\n    <ngb-alert *ngIf=\"showalert\" [dismissible]=\"false\" type=\"success\">{{ successMessage }}</ngb-alert>\r\n  </div>\r\n</div>\r\n<br>\r\n<div class=\"container\">\r\n  <div class=\"row\">\r\n    <div id=\"main-content\" class=\"container\">\r\n      <div class=\"row\">\r\n        <div class=\"col-md-6\">\r\n          <form class=\"form-inline\">\r\n            <div class=\"form-group\">\r\n              <label for=\"playercount\">Game info:</label>\r\n              <div id=\"playercount\" class=\"\"></div>\r\n            </div>\r\n          </form>\r\n        </div>\r\n        <div class=\"col-md-6\">\r\n          <button id=\"startgame\" (click)=\"startgame()\"  class=\"btn btn-primary btn-lg\" type=\"submit\">Start Game!</button>\r\n          <hr>\r\n          <button id=\"resetgame\" (click)=\"resetgame()\"  class=\"btn btn-danger btn-lg\" type=\"submit\">Reset Game!</button>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n<br>\r\n<div class=\"container\">\r\n  <div class=\"row\">\r\n    <div class=\"col-2\">\r\n      <h1>A</h1>\r\n    </div>\r\n    <div class=\"col-8\">\r\n      <ngb-progressbar [value]=\"teamAvalue\"  type=\"info\"></ngb-progressbar>\r\n    </div>\r\n  </div>\r\n</div>\r\n<div class=\"container\">\r\n  <div class=\"row\">\r\n    <br>\r\n  </div>\r\n</div>\r\n<div class=\"container\">\r\n  <div class=\"row\">\r\n    <div class=\"col-2\">\r\n      <h1>B</h1>\r\n    </div>\r\n    <div class=\"col-8\">\r\n      <ngb-progressbar [value]=\"teamBvalue\"></ngb-progressbar>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n"

/***/ }),

/***/ "../../../../../src/app/game-config-client/game-config-client.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GameConfigClientComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__stomp_ng2_stompjs__ = __webpack_require__("../../../../@stomp/ng2-stompjs/@stomp/ng2-stompjs.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_game_api_service__ = __webpack_require__("../../../../../src/app/services/game-api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Subject__ = __webpack_require__("../../../../rxjs/_esm5/Subject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_operator_debounceTime__ = __webpack_require__("../../../../rxjs/_esm5/operator/debounceTime.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ng_bootstrap_ng_bootstrap__ = __webpack_require__("../../../../@ng-bootstrap/ng-bootstrap/index.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var GameConfigClientComponent = (function () {
    function GameConfigClientComponent(_stompService, stageServices, config) {
        var _this = this;
        this._stompService = _stompService;
        this.stageServices = stageServices;
        this.showalert = false;
        this._success = new __WEBPACK_IMPORTED_MODULE_3_rxjs_Subject__["a" /* Subject */]();
        this.teamAvalue = 0;
        this.teamBvalue = 0;
        this.on_next = function (message) {
            console.log(message.body);
            var msg = JSON.parse(message.body);
            switch (msg.type) {
                case 'Game_Score':
                    _this.teamAvalue = msg.teamA;
                    _this.teamBvalue = msg.teamB;
                    break;
            }
        };
        config.max = 1000;
        config.striped = true;
        config.animated = true;
        config.type = 'success';
        config.height = '150px';
        config.showValue = true;
    }
    GameConfigClientComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.subscribed = false;
        this.subscribe();
        this._success.subscribe(function (message) {
            _this.successMessage = message;
            _this.showalert = true;
        });
        __WEBPACK_IMPORTED_MODULE_4_rxjs_operator_debounceTime__["a" /* debounceTime */].call(this._success, 5000).subscribe(function () { return _this.showalert = false; });
    };
    GameConfigClientComponent.prototype.ngOnDestroy = function () {
        this.unsubscribe();
    };
    GameConfigClientComponent.prototype.subscribe = function () {
        if (this.subscribed) {
            return;
        }
        this.messages = this._stompService.subscribe('/topic/game');
        this.subscription = this.messages.subscribe(this.on_next);
        this.subscribed = true;
    };
    GameConfigClientComponent.prototype.unsubscribe = function () {
        if (!this.subscribed) {
            return;
        }
        // This will internally unsubscribe from Stomp Broker
        // There are two subscriptions - one created explicitly, the other created in the template by use of 'async'
        this.subscription.unsubscribe();
        this.subscription = null;
        this.messages = null;
        this.subscribed = false;
    };
    GameConfigClientComponent.prototype.showAlertGameStarted = function () {
        // console.log('show alert!');
        this._success.next('The game has started!');
    };
    GameConfigClientComponent.prototype.startgame = function () {
        this.stageServices.startGame()
            .toPromise()
            .then(function () { return console.log('started'); });
    };
    GameConfigClientComponent.prototype.resetgame = function () {
        this.stageServices.resetGame()
            .toPromise()
            .then(function () { return console.log('reset'); });
    };
    GameConfigClientComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-game-config-client',
            template: __webpack_require__("../../../../../src/app/game-config-client/game-config-client.component.html")
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__stomp_ng2_stompjs__["b" /* StompService */], __WEBPACK_IMPORTED_MODULE_2__services_game_api_service__["a" /* GameService */], __WEBPACK_IMPORTED_MODULE_5__ng_bootstrap_ng_bootstrap__["b" /* NgbProgressbarConfig */]])
    ], GameConfigClientComponent);
    return GameConfigClientComponent;
}());



/***/ }),

/***/ "../../../../../src/app/progress-bar-animated/progress-bar-animated.component.html":
/***/ (function(module, exports) {

module.exports = "<p><ngb-progressbar type=\"info\"></ngb-progressbar></p>\r\n\r\n"

/***/ }),

/***/ "../../../../../src/app/progress-bar-animated/progress-bar-animated.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProgressBarAnimatedComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__ = __webpack_require__("../../../../@ng-bootstrap/ng-bootstrap/index.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ProgressBarAnimatedComponent = (function () {
    function ProgressBarAnimatedComponent(config) {
        // customize default values of progress bars used by this component tree
        config.max = 1000;
        config.striped = true;
        config.animated = true;
        config.type = 'success';
        config.height = '50px';
        config.showValue = true;
    }
    ProgressBarAnimatedComponent.prototype.ngOnInit = function () {
    };
    ProgressBarAnimatedComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-progress-bar-animated',
            template: __webpack_require__("../../../../../src/app/progress-bar-animated/progress-bar-animated.component.html")
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__["b" /* NgbProgressbarConfig */]])
    ], ProgressBarAnimatedComponent);
    return ProgressBarAnimatedComponent;
}());



/***/ }),

/***/ "../../../../../src/app/services/game-api.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GameService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__("../../../common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var GameService = (function () {
    function GameService(http) {
        this.http = http;
    }
    GameService.prototype.getTeam = function () {
        return this.http.get('/getTeam');
    };
    GameService.prototype.startGame = function () {
        return this.http.get('/startgame');
    };
    GameService.prototype.resetGame = function () {
        return this.http.get('/reset');
    };
    GameService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], GameService);
    return GameService;
}());



/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false
};


/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_15" /* enableProdMode */])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map