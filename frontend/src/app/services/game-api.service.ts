import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import {Injectable} from '@angular/core';


@Injectable()
export class GameService {

  constructor(private http: HttpClient) {

  }

  public getTeam(): Observable<any> {
    return this.http.get('/getTeam');
  }

  public startGame(): Observable<any> {
    return this.http.get('/startgame');
  }

  public resetGame(): Observable<any> {
    return this.http.get('/reset');
  }
}

